package lover.storageadmin.Database;

import lover.storageadmin.Models.Dialog;
import lover.storageadmin.Models.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Crud {
    public static Product insertProduct() {
        Product product = new Product(0, "", "", 0, 0.0, 0.0);
        try {
            Connect connect = Connect.getInstance();
            PreparedStatement psCheck;
            ResultSet resultSet;
            Connection connection = connect.getConnection();
            psCheck = connection.prepareStatement("insert into Product(name, description, quantity, priceGet, priceSell) VALUE('', '', 0, 0.0, 0.0)");
            psCheck.executeUpdate();
            psCheck = connection.prepareStatement("SELECT MAX(id) as id FROM Product");
            resultSet = psCheck.executeQuery();
            while (resultSet.next()) {
                product.setId(resultSet.getInt("id"));
            }
            resultSet.close();
            psCheck.close();
        } catch (SQLException ex) {
            Dialog.error("INSERT FAILED.");
        }
        return product;
    }

    public static void deleteProduct(int productId) {
        try {
            Connect connect = Connect.getInstance();
            PreparedStatement psCheck;
            Connection connection = connect.getConnection();
            psCheck = connection.prepareStatement("DELETE FROM Product WHERE id = ?");
            psCheck.setInt(1, productId);
            psCheck.executeUpdate();
            psCheck.close();
        } catch (SQLException ex) {
            Dialog.error("DELETE FAILED.");
        }
    }

    public static void updateName(int productId, String productName) {
        try {
            Connect connect = Connect.getInstance();
            PreparedStatement psCheck;
            Connection connection = connect.getConnection();
            psCheck = connection.prepareStatement("UPDATE Product set name = ? where id = ?");
            psCheck.setString(1, productName);
            psCheck.setInt(2, productId);
            psCheck.executeUpdate();
            psCheck.close();
        } catch (SQLException ex) {
            Dialog.error("UPDATE NAME FAILED.");
        }
    }

    public static void updateDescription(int productId, String productDescription) {
        try {
            Connect connect = Connect.getInstance();
            PreparedStatement psCheck;
            Connection connection = connect.getConnection();
            psCheck = connection.prepareStatement("UPDATE Product set description = ? where id = ?");
            psCheck.setString(1, productDescription);
            psCheck.setInt(2, productId);
            psCheck.executeUpdate();
            psCheck.close();
        } catch (SQLException ex) {
            Dialog.error("UPDATE DESCRIPTION FAILED.");
        }
    }

    public static void updateQuantity(int productId, int productQuantity) {
        try {
            Connect connect = Connect.getInstance();
            PreparedStatement psCheck;
            Connection connection = connect.getConnection();
            psCheck = connection.prepareStatement("UPDATE Product set quantity = ? where id = ?");
            psCheck.setInt(1, productQuantity);
            psCheck.setInt(2, productId);
            psCheck.executeUpdate();
            psCheck.close();
        } catch (SQLException ex) {
            Dialog.error("UPDATE QUANTITY FAILED.");
        }
    }

    public static void updateShipPrice(int productId, double shipPrice) {
        try {
            Connect connect = Connect.getInstance();
            PreparedStatement psCheck;
            Connection connection = connect.getConnection();
            psCheck = connection.prepareStatement("UPDATE Product set priceGet = ? where id = ?");
            psCheck.setDouble(1, shipPrice);
            psCheck.setInt(2, productId);
            psCheck.executeUpdate();
            psCheck.close();
        } catch (SQLException ex) {
            Dialog.error("UPDATE SHIP PRICE FAILED.");
        }
    }

    public static void updateSellPrice(int productId, double sellPrice) {
        try {
            Connect connect = Connect.getInstance();
            PreparedStatement psCheck;
            Connection connection = connect.getConnection();
            psCheck = connection.prepareStatement("UPDATE Product set priceSell = ? where id = ?");
            psCheck.setDouble(1, sellPrice);
            psCheck.setInt(2, productId);
            psCheck.executeUpdate();
            psCheck.close();
        } catch (SQLException ex) {
            Dialog.error("UPDATE SELL PRICE FAILED.");
        }
    }
}
