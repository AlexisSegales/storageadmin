package lover.storageadmin.Database;

import lover.storageadmin.Models.Dialog;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {
    private Connection connection;
    private static Connect connect;

    private Connect() {
        try {
            String username = "urc0vdhdqyglmoy2";
            String password = "rdZ99jbnUjV88SUOIkP2";
            String url = "jdbc:mysql://b4vlbcnxftyteo83zbiz-mysql.services.clever-cloud.com:3306/b4vlbcnxftyteo83zbiz";
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException exception) {
            Dialog.error("Not connected to DB.");
        }
    }

    public static Connect getInstance() {
        if (connect == null) {
            connect = new Connect();
        }
        return connect;
    }

    public Connection getConnection() {
        if (connection == null) {
            Dialog.error("Connection is null");
        }
        return connection;
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }

}
