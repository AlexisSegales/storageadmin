package lover.storageadmin.Repository;

import lover.storageadmin.Models.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductRepository {
    private final List<Product> products;
    private static ProductRepository productRepository;
    private static List<Product> filteredProducts;
    private static String reducedTitle;

    private ProductRepository() {
        products = new ArrayList<>();
        filteredProducts = new ArrayList<>();
    }

    public static ProductRepository getInstance() {
        if (productRepository == null) {
            productRepository = new ProductRepository();
        }
        return productRepository;
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public List<Product> getAllProducts() {
        return products;
    }

    public void eraseProduct(int productId) {
        products.removeIf(product -> product.getId() == productId);
    }

    public List<Product> getFilteredProducts(String word, int size) {
        filteredProducts.clear();
        word = word.toLowerCase();
        System.out.println("Word: "+ word + " | "+ size);
        for (Product item : products) {
            reducedTitle = item.getName().substring(0,size).toLowerCase();
            System.out.println("Es igual: " + reducedTitle.equals(word) + " | "+ reducedTitle);
            if (reducedTitle.equals(word)) {
                filteredProducts.add(item);
            }
        }
        return filteredProducts;
    }
}
