module lover.storageadmin {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires net.synedra.validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires eu.hansolo.tilesfx;
    requires java.sql;

    opens lover.storageadmin to javafx.fxml;
    exports lover.storageadmin;
    exports lover.storageadmin.Models;
    exports lover.storageadmin.Controllers;
    opens lover.storageadmin.Controllers to javafx.fxml;
}