package lover.storageadmin.Controllers;

import javafx.fxml.FXML;
import javafx.scene.text.Text;
import lover.storageadmin.Models.Product;

public class ProductView {
    @FXML
    private Text tittle;
    @FXML
    private Text quantity;


    public void setProduct(Product product) {
        tittle.setText(product.getName());
        quantity.setText(String.valueOf(product.getQuantity()));
    }
}
