package lover.storageadmin.Controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import lover.storageadmin.App;
import lover.storageadmin.Database.Crud;
import lover.storageadmin.Models.Dialog;
import lover.storageadmin.Models.Product;
import lover.storageadmin.Repository.ProductRepository;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class MainPage {

    @FXML
    private VBox productContainer;

    @FXML
    private AnchorPane productView;

    ProductRepository productRepository;
    private static int size = 0;
    private static String word = "";

    @FXML
    void research(KeyEvent event) {
        word = word.concat(event.getText());
        if (event.getCode() == KeyCode.BACK_SPACE) {
            if (size > 0) {
                size--;
            }
        } else {
            size++;
        }
        listOfProducts(productRepository.getFilteredProducts(word, size));
    }

    @FXML
    void initialize() {
        productRepository = ProductRepository.getInstance();
        listOfProducts(productRepository.getAllProducts());
    }

    @FXML
    void addProduct() {
        Product product = Crud.insertProduct();
        productRepository.addProduct(product);
        showProduct(product);
    }

    public void listOfProducts(List<Product> products) {
        productContainer.getChildren().clear();
        for (Product product : products) {
            showProduct(product);
        }

    }

    public void showProduct(Product product) {
        productContainer.setPadding(new Insets(10));
        productContainer.setAlignment(Pos.TOP_CENTER);
        productContainer.setSpacing(10);
        try {
            productContainer.getChildren().add(productOnScreen(product));
        } catch (IOException ex) {
            Dialog.error("No se puede imprimir");
        }
    }

    public AnchorPane productOnScreen(Product product) throws IOException {
        FXMLLoader box = new FXMLLoader(App.class.getResource("ProductView.fxml"));
        AnchorPane item = box.load();
        ProductView pvc = box.getController();
        pvc.setProduct(product);

        ImageView imageView = getImageView(product);
        item.getChildren().add(imageView);

        item.setOnMouseClicked(event -> {
            try {
                FXMLLoader productInfo = new FXMLLoader(App.class.getResource("ProductInfo.fxml"));
                AnchorPane productWindow = productInfo.load();
                ProductInfo pic = productInfo.getController();
                pic.setProduct(product);
                pic.setProductView(pvc);
                productView.getChildren().add(productWindow);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        return item;
    }

    private ImageView getImageView(Product product) throws FileNotFoundException {
        ImageView imageView = new ImageView(new Image(new FileInputStream("src/main/resources/lover/storageadmin/images/trash.png")));
        imageView.setFitHeight(30.0);
        imageView.setFitWidth(40.0);
        imageView.setLayoutX(550.0);
        imageView.setLayoutY(20.0);
        imageView.setPickOnBounds(true);
        imageView.setPreserveRatio(true);

        imageView.setOnMouseClicked(e -> {
            Crud.deleteProduct(product.getId());
            productRepository.eraseProduct(product.getId());
            listOfProducts(productRepository.getAllProducts());
        });
        return imageView;
    }
}
