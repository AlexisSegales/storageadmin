package lover.storageadmin.Controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import lover.storageadmin.Database.Crud;
import lover.storageadmin.Models.Dialog;
import lover.storageadmin.Models.Product;

public class ProductInfo {

    @FXML
    private Text description;

    @FXML
    private TextField editDescription;

    @FXML
    private TextField editName;

    @FXML
    private TextField editQuantity;

    @FXML
    private TextField editSellPrice;

    @FXML
    private TextField editShipPrice;

    @FXML
    private Text name;

    @FXML
    private Text quantity;

    @FXML
    private Text sellPrice;

    @FXML
    private Text shipPrice;

    private Product product;
    private ProductView productView;

    @FXML
    void enterOnDescription(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            String description = editDescription.getText();
            if (!description.isEmpty()) {
                product.setDescription(description);
                this.description.setText(product.getDescription());
                productView.setProduct(product);
                Crud.updateDescription(product.getId(), product.getDescription());
                editDescription.clear();
            }
        }
    }

    @FXML
    void enterOnName(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            String name = editName.getText();
            if (!name.isEmpty()) {
                product.setName(name);
                this.name.setText(product.getName());
                productView.setProduct(product);
                Crud.updateName(product.getId(), product.getName());
                editName.clear();
            }
        }
    }

    @FXML
    void enterOnQuantity(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            String quantity = editQuantity.getText();
            if (!quantity.isEmpty()) {
                int valor = isIntNumber(quantity);
                product.setQuantity(valor);
                this.quantity.setText(String.valueOf(product.getQuantity()));
                productView.setProduct(product);
                Crud.updateQuantity(product.getId(), product.getQuantity());
                editQuantity.clear();
            }
        }
    }

    @FXML
    void enterOnSellPrice(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            String sellPrice = editSellPrice.getText();
            if (!sellPrice.isEmpty()) {
                double valor = isDoubleNumber(sellPrice);
                product.setPriceSell(valor);
                this.sellPrice.setText(String.valueOf(product.getPriceSell()));
                productView.setProduct(product);
                Crud.updateSellPrice(product.getId(), product.getPriceSell());
                editSellPrice.clear();
            }
        }
    }

    @FXML
    void enterOnShipPrice(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            String shipPrice = editShipPrice.getText();
            if (!shipPrice.isEmpty()) {
                double valor = isDoubleNumber(shipPrice);
                product.setPriceGet(valor);
                this.shipPrice.setText(String.valueOf(product.getPriceGet()));
                productView.setProduct(product);
                Crud.updateShipPrice(product.getId(), product.getPriceGet());
                editShipPrice.clear();
            }
        }
    }

    public void setProductView(ProductView productView) {
        this.productView = productView;
    }

    public int isIntNumber(String number) {
        int realNumber = 0;
        try {
            realNumber = Integer.parseInt(number);
        } catch (NumberFormatException ex) {
            Dialog.error("El parametro debe ser un numero entero.");
        }
        return realNumber;
    }

    public double isDoubleNumber(String number) {
        double realNumber = 0;
        try {
            realNumber = Double.parseDouble(number);
        } catch (NumberFormatException ex) {
            Dialog.error("El parametro debe ser un numero decimal.");
        }
        return realNumber;
    }

    void setProduct(Product product) {
        this.product = product;
        name.setText(product.getName());
        quantity.setText(String.valueOf(product.getQuantity()));
        sellPrice.setText(String.valueOf(product.getPriceSell()));
        shipPrice.setText(String.valueOf(product.getPriceGet()));
        description.setText(product.getDescription());
    }

}
