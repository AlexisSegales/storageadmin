package lover.storageadmin.Controllers;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lover.storageadmin.App;
import lover.storageadmin.Database.Connect;
import lover.storageadmin.Models.Dialog;
import lover.storageadmin.Models.Product;
import lover.storageadmin.Repository.ProductRepository;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class WelcomePage {
    private Stage stage;

    @FXML
    private AnchorPane background;

    public void goToMainPage() {
        stage = (Stage) this.background.getScene().getWindow();

        Task<Void> gotoMainPage = new Task<>() {
            @Override
            public Void call() {
                Connect.getInstance();
                return null;
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                try {
                    ProductRepository productRepository = ProductRepository.getInstance();
                    Connect connect = Connect.getInstance();
                    PreparedStatement psCheck;
                    ResultSet resultSet;

                    Connection connection = connect.getConnection();
                    psCheck = connection.prepareStatement("SELECT * FROM Product");
                    resultSet = psCheck.executeQuery();
                    while (resultSet.next()) {
                        int id = resultSet.getInt("id");
                        String name = resultSet.getString("name");
                        String description = resultSet.getString("description");
                        int quantity = resultSet.getInt("quantity");
                        double priceGet = resultSet.getDouble("priceGet");
                        double priceSell = resultSet.getDouble("priceSell");
                        Product product = new Product(id, name, description, quantity, priceGet, priceSell);
                        productRepository.addProduct(product);
                    }
                    resultSet.close();
                    psCheck.close();
                } catch (SQLException ex) {
                    Dialog.error("DATABASE CONNECTION LOST.");
                }
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("MainPage.fxml"));
                    Parent root = FXMLLoader.load(fxmlLoader.getLocation());
                    stage.setScene(new Scene(root));
                    stage.show();
                } catch (IOException ex) {
                    Dialog.error("Main Page not available.");
                }
            }

            @Override
            protected void failed() {
                super.failed();
                Dialog.error("Could not reach to Database.");
            }

        };

        new Thread(gotoMainPage).start();
    }
}
