package lover.storageadmin;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lover.storageadmin.Controllers.WelcomePage;

import java.io.IOException;

public class App extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("WelcomePage.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1280, 640);
        stage.setTitle("Lover Shop");
        stage.setScene(scene);
        stage.show();
        WelcomePage welcomePage = fxmlLoader.getController();
        welcomePage.goToMainPage();
    }

    public static void main(String[] args) {
        launch();
    }
}