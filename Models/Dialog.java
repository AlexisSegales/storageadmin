package lover.storageadmin.Models;

import javafx.scene.control.Alert;

public class Dialog {
    public static void error(String reason) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("ERROR");
        alert.setHeaderText(null);
        alert.setContentText(reason);
        alert.showAndWait();
    }

    public static void information(String reason) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("INFORMATION");
        alert.setHeaderText(null);
        alert.setContentText(reason);
        alert.showAndWait();
    }
}
