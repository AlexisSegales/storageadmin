package lover.storageadmin.Models;

public class Product {
    private int id;
    private String name;
    private String description;
    private int quantity;
    private double priceGet;
    private double priceSell;

    public Product(int id, String name, String description, int quantity, double priceGet, double priceSell) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.quantity = quantity;
        this.priceGet = priceGet;
        this.priceSell = priceSell;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPriceGet() {
        return priceGet;
    }

    public void setPriceGet(double priceGet) {
        this.priceGet = priceGet;
    }

    public double getPriceSell() {
        return priceSell;
    }

    public void setPriceSell(double priceSell) {
        this.priceSell = priceSell;
    }
}
